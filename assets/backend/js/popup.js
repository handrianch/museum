$(".delete-btn").on("click", function(){
    let id = $(this).parent().parent().data("set-id");
    let form = $("#set-delete");
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            form.attr('action', `${baseURL}/${id}/delete`);
            form.submit();
        }
    });
});