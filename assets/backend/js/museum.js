function readURL(input) {
	if (input.files && input.files[0]) {
    	var reader = new FileReader();

    	reader.onload = function (e) {
    		$("#img-title").text("Image Changed")
        	$('#gambar-thumb').attr('src', e.target.result);
        	let imgName = input.files[0].name.substr(0, 10);
        	$('#judul-gambar').text(imgName);
    	}

    	reader.readAsDataURL(input.files[0]);
	}
}

$("#gambar").change(function () {
    readURL(this);
});

CKEDITOR.replace('keterangan');
CKEDITOR.replace('alamat');
CKEDITOR.replace('jadwal-buka-tutup');
CKEDITOR.replace('harga-tiket');