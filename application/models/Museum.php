<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Museum extends CI_Model
{
	public function __construct()
	{
		parent::__construct();

		$this->load->database();
	}

	public function all($limit = 0, $start = 0)
	{
		return $this->db->get("museum", $limit, $start)->result();
	}

	public function get($arg, $column = 'id')
	{
		return $this->db->where($column, $arg)->get("museum")->row();
	}

	public function save($data)
	{
		return $this->db->insert('museum', $data);
	}

	public function update($arg, $data)
	{
		return $this->db->where('id', $arg)->update('museum', $data); 
	}

	public function destroy($arg)
	{
		$data = $this->db->where('id', $arg)->get("museum")->result();

		if(count($data) <= 0) {
			return false;
		}

		return $this->db->where('id', $arg)->delete("museum");
	}

	public function rowCount()
	{
		return $this->db->get("museum")->num_rows();
	}
}