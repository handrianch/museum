<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class User extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function all($limit = 0, $start = 0)
	{
		return $this->db->get("users", $limit, $start)->result();
	}

	public function get($arg, $column = 'id')
	{
		return $this->db->where($column, $arg)->get("users")->row();
	}

	public function save($data)
	{
		return $this->db->insert("users", $data);
	}

	public function update($arg, $data)
	{
		return $this->db->where('id', $arg)->update('users', $data);
	}

	public function destroy($arg)
	{
		$data = $this->db->where('id', $arg)->get("users")->result();

		if(count($data) <= 0) {
			return false;
		}
		return $this->db->where('id', $arg)->delete("users");
	}

	public function rowCount()
	{
		return $this->db->get("users")->num_rows();
	}
}