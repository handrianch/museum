<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Role extends CI_Model
{
	public function __construct()
	{
		parent::__construct();

		$this->load->database();
	}

	public function all($limit = 0, $start = 0)
	{
		return $this->db->get("role")->result();
	}

	public function get($arg)
	{
		return $this->db->where('id', $arg)->get("role")->row();
	}
}