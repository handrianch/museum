<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Area extends CI_Model
{
	public function __construct()
	{
		parent::__construct();

		$this->load->database();
	}

	public function all($limit = 0, $start = 0, $join = false)
	{
		$this->db->select("area.*, users.nama");
		$this->db->join("users", "users.id = area.id_user");
		return $this->db->get("area", $limit, $start)->result();
	}

	public function get($arg)
	{
		return $this->db->where('id', $arg)->get("area")->row();
	}

	public function save($data)
	{
		return $this->db->insert('area', $data);
	}

	public function update($arg, $data)
	{
		return $this->db->where('id', $arg)->update('area', $data);
	}

	public function destroy($arg)
	{
		$data = $this->db->where('id', $arg)->get("area")->result();

		if(count($data) <= 0) {
			return false;
		}
		return $this->db->where('id', $arg)->delete('area');
	}

	public function rowCount()
	{
		return $this->db->get("area")->num_rows();
	}
}