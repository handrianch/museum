<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Setting extends CI_Model
{
  public function __construct()
  {
    parent::__construct();

    $this->load->database();
  }

  public function all()
  {
    $this->db->select("settings.*, users.nama");
    $this->db->join("users", "users.id = settings.changed_by");
    return $this->db->where("settings.id", 1)->get("settings")->row();
  }

  public function get()
  {
    return $this->db->where("settings.id", 1)->get("settings")->row(); 
  }

  public function save($data)
  {
    $settings = $this->db->where('id', 1)->get("settings")->row();

    if(count($settings) <= 0) {
      return $this->db->insert('settings', $data);
    } else {
      return $this->db->where('id', 1)->update('settings', $data);
    }
    
  }

}