<?php
defined("BASEPATH") OR exit("No direct script access allowed");

use Jenssegers\Blade\Blade;

if(!function_exists('view')) {
	function view($view, $data = []) {
		$view = preg_replace('/\./', '/', $view);
		$path = APPPATH . 'views';
		$blade = new Blade($path, $path . '\cache');

		echo $blade->make($view, $data);
	}
}

if(!function_exists("getFlashMessage")) {
	function getFlashMessage() {
		$CI =& get_instance();
		$method = $CI->router->fetch_method();
		
		$CI->data["login"] = $CI->session->userdata("login");

		if($method !== 'save' && $method !== 'update' && $method !== 'destroy') {
			if($CI->session->flashdata("type") && $CI->session->flashdata("message")) {
				$CI->data["type"] = $CI->session->flashdata("type");
				$CI->data["message"] = $CI->session->flashdata("message");
			}

			if($CI->session->flashdata("errData")) {
				$CI->data["errData"] = $CI->session->flashdata("errData");
			}
		}
	}
}