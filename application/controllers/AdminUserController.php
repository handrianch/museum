<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class AdminUserController extends CI_Controller
{
	public $data = [];

	public function __construct()
	{
		parent::__construct();

		$this->load->model("User");
		$this->load->model("Setting");

		$this->middleware->auth();
		$this->middleware->protect("user", "users");

		$this->data['settings'] = $this->Setting->get();
		$this->data['bgColor'] = 'bg-warning';
		$this->data['page'] = 'Users';
		
		getFlashMessage();
	}

	public function index()
	{
		$this->load->library("Paginate");

		$pagination = $this->paginate->makeIt(base_url("dashboard/users"), $this->User->rowCount());
		
		$this->data['pagination'] = $pagination['pagination'];
		$this->data['start'] = $pagination['start'];
		$this->data['users'] = $this->User->all($pagination['per_page'], $pagination['page']);

		return view("admin.users.index", $this->data);
	}

	public function create()
	{
		$this->load->model("Role");

		$this->data['action'] = base_url() . "dashboard/users/save";
		$this->data['roleList'] = $this->Role->all();
	
		return view("admin.users.form", $this->data);
	}

	public function save()
	{
		$this->load->library("FormValidation");

		$message = "Failed creating data";
		$type = 'error';

		if(strtoupper($this->input->method()) === "POST") {
			
			$role = ($this->session->userdata("login")['id'] == 1) ? $this->input->post("role") : '';

			if(empty($role)) {
				$role = 2;
			}

			$data = [
				'nama' => $this->input->post("nama"),
				'email' => $this->input->post("email"),
				'role_id' => $role,
			];

			if($this->formvalidation->validationRules('user')) {

				$data["password"] = password_hash($this->input->post("password"), PASSWORD_BCRYPT);

				if($this->User->save($data)) {
					$type = 'success';
					$message = 'Data saved successfully';
				}
			} else {
				$errData = $this->formvalidation->getErrMsg('user', $data);
			}
		}

		$this->session->set_flashdata('type', $type);
		$this->session->set_flashdata('message', $message);

		if(isset($errData)) {
			$this->session->set_flashdata("errData", (object)$errData);
			return redirect("dashboard/users/add");
		}

		return redirect("dashboard/users");
	}

	public function edit($id)
	{
		$this->load->model("Role");

		$this->data["users"] = $this->User->get($id);
		$this->data['action'] = base_url() . "dashboard/users/$id/update";
		$this->data['roleList'] = $this->Role->all();

		return view("admin.users.form", $this->data);
	}

	public function update($id)
	{
		$this->load->library("FormValidation");

		$message = "Failed updating user";
		$type = 'error';

		$tempData['email'] = false;
		$tempData['password'] = true;

		if(strtoupper($this->input->method()) === "POST") {
			
			$role = 2;
			
			if($this->session->userdata("login")['role'] == 1 && $this->session->userdata("login")['id'] == 1) {
				$role = $this->input->post("role");
			}

			$data = [
				'nama' => $this->input->post("nama"),
				'email' => $this->input->post("email"),
				'role_id' => $role,
			];

			$user = $this->User->get($id);

			if($user->email !== $data['email']) {
				$tempData['email'] = true;
			}

			if(empty($this->input->post('password'))) {
				$tempData['password'] = false;
			}

			if($this->formvalidation->validationRules('user', $tempData['email'], $tempData['password'])) {

				if($tempData['password']) {
					$data['password'] = password_hash($this->input->post("password"), PASSWORD_BCRYPT);
				}

				if($this->User->update($id, $data)) {
					$type = 'success';
					$message= 'Data updated successfully';
				}

			} else {
				$errData = $this->formvalidation->getErrMsg('user', $data);
				$message = "Failed updating data user $user->nama";
			}
		}

		if($tempData['password']) {
			$user = $this->User->get($id);
			$userSess = $this->session->userdata("login");
			
			if($user->id == $userSess['id']) {
				$this->session->sess_destroy();
			}
		}

		$this->session->set_flashdata('type', $type);
		$this->session->set_flashdata('message', $message);

		if(isset($errData)) {
			$this->session->set_flashdata("errData", (object)$errData);
			return redirect("dashboard/users/$id/edit");
		}

		return redirect("dashboard/users");
	}

	public function delete($id)
	{
		$data = false;
		$message = "Failed deleting user";
		$type = "error";

		if(strtoupper($this->input->method()) === "POST") {
			if(isset($id) && !empty($id)) {
				$data = $this->User->destroy($id);
			}

			if($data) {
				$type = 'success';
				$message = 'Data deleted successfully';
			}
		}

		$this->session->set_flashdata('type', $type);
		$this->session->set_flashdata('message', $message);

		return redirect("dashboard/users");
	}

}
