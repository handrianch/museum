<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class SettingsController extends CI_Controller
{
  public $data = [];

  public function __construct()
  {
    parent::__construct();
    
    $this->middleware->auth();

    $this->load->model("Setting");

    $this->data['bgColor'] = 'bg-danger';
    $this->data['page'] = 'Web Settings';

    getFlashMessage();
  }

  public function index()
  {
    $this->data['settings'] = $this->Setting->all();
    $this->data['action'] = base_url("dashboard/settings/save");

    return view("admin.settings", $this->data);
  }

  public function config()
  { 
    $this->load->library("FormValidation");

    $message = "Failed creating data";
    $type = 'error';

    if(strtoupper($this->input->method()) == "POST") {
      
      $data = [
        'nama_website' => $this->input->post("nama_website"),
        'home_text' => $this->input->post("home"),
        'about_text' => $this->input->post("about"),
        'contact_text' => $this->input->post("contact"),
        'changed_by' => $this->session->userdata("login")['id']
      ];

      if($this->formvalidation->validationRules('settings')) {
      
        if($this->Setting->save($data)) {
          $type = 'success';
          $message = 'Data saved successfully';
        }

      } else {
        $errData = $this->formvalidation->getErrMsg('settings', $data);
      }
    }

    $this->session->set_flashdata('type', $type);
    $this->session->set_flashdata('message', $message);

    if(isset($errData)) {
      $this->session->set_flashdata("errData", (object)$errData);
    }

    return redirect("dashboard/settings");
  }

}