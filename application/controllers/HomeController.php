<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class HomeController extends CI_Controller
{
	private $data = [];

	public function __construct()
	{
		parent::__construct();

		$this->load->model("Setting");
		$this->data['settings'] = $this->Setting->get();
	}

	public function index()
	{
		$settings = $this->data['settings'];
		$homeText = preg_replace("/<p>/", '', $settings->home_text);
		$homeText = preg_replace("/<\/p>/", '', $homeText);
		$this->data['settings']->home_text = $homeText;
		$this->data['page'] = 'home';

		return view('client.index', $this->data);
	}

	public function about()
	{
		$settings = $this->data['settings'];
		$aboutText = preg_replace("/<p>/", '', $settings->about_text);
		$aboutText = preg_replace("/<\/p>/", '', $aboutText);
		$this->data['settings']->about_text = $aboutText;
		$this->data['page'] = 'tentang';

		return view('client.tentang', $this->data);
	}

	public function contact()
	{
		$settings = $this->data['settings'];
		$contactText = preg_replace("/<p>/", '', $settings->contact_text);
		$contactText = preg_replace("/<\/p>/", '', $contactText);
		$this->data['settings']->contact_text = $contactText;
		$this->data['page'] = 'kontak';

		return view('client.kontak', $this->data);
	}
}