<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class DashboardController extends CI_Controller
{
	public $data = [];

	public function __construct()
	{
		parent::__construct();
		
		$this->middleware->auth();

		$this->load->model("Area");
		$this->load->model("Museum");
		$this->load->model("User");
		$this->load->model("Setting");

		$this->data['page'] = 'Dashboard';
		$this->data['bgColor'] = 'bg-danger';

		getFlashMessage();
	}

	public function index()
	{
		$data = [
			'usersCount' => $this->User->rowCount(),
			'museumCount' => $this->Museum->rowCount(),
			'areaCount' => $this->Area->rowCount()
		];

		$this->data = array_merge($this->data, $data);
		$this->data['settings'] = $this->Setting->get();
		
		return view("admin.index", $this->data);
	}
}