<?php
defined("BASEPATH") OR exit("No direct access script allowed");

class AuthenticationController extends CI_Controller
{
	public $data = [];

	public function __construct()
	{
		parent::__construct();

		$this->middleware->guest(['logout']);
		$this->load->library("Auth");
		$this->load->library("FormValidation");
	}

	public function index()
	{
		getFlashMessage();
		
		$this->data['action'] = base_url() . 'attempt';

		return view("admin.login", $this->data);
	}

	public function login()
	{
		$message = 'Login First!';
		$type = 'error';

		if(strtoupper($this->input->method()) === "POST") {
			$data = [
				'email' => $this->input->post('email'),
				'password' => $this->input->post('password'),
			];

			if($this->formvalidation->validationRules('auth')) {
				if($this->auth->attempt($data)) {
					return redirect("dashboard");
				} else {
					$errData = $this->auth->getErrMsg();
				}
			} else {
				$errData = $this->formvalidation->getErrMsg('auth', $data);
			}

		}

		if(isset($errData)) {
			$type = 'error';
			$message = 'Login Failed! Check email and password';
			$this->session->set_flashdata("errData", (object)$errData);
		}

		$this->session->set_flashdata('type', $type);
		$this->session->set_flashdata('message', $message);
		return redirect("login");
	}

	public function logout()
	{
	 	if($this->auth->logout()) {
	 		return redirect("/");	
	 	}
		return redirect("dashboard");
	}
}