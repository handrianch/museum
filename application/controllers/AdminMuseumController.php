<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class AdminMuseumController extends CI_Controller
{
	public $data = [];

	public function __construct()
	{
		parent::__construct();

		$this->load->model("Museum");
		$this->load->model("Area");
		$this->load->model("Setting");

		$this->middleware->auth();
		$this->middleware->protect("museum", "museum");

		$this->data['settings'] = $this->Setting->get();
		$this->data['bgColor'] = 'bg-primary';
		$this->data['page'] = 'Museum';

		getFlashMessage();
	}

	public function index()
	{
		$this->load->library("Paginate");

		$pagination = $this->paginate->makeIt(base_url("dashboard/museum"), $this->Museum->rowCount());
		
		$this->data['pagination'] = $pagination['pagination'];
		$this->data['start'] = $pagination['start'];
		$this->data['allMuseum'] = $this->Museum->all($pagination['per_page'], $pagination['page']);

		return view("admin.museum.index", $this->data);
	}

	public function create()
	{
		$this->data['action'] = base_url("dashboard/museum/save");
		$this->data['area'] = $this->Area->all();
		return view("admin.museum.form", $this->data);
	}

	public function save()
	{
		$this->load->library("FormValidation");

		$message = "Failed creating data";
		$type = 'error';

		if(strtoupper($this->input->method()) === "POST") {
			
			$gambar = $this->uploadImg();

			if($this->input->post("harga_tiket")) {
				$hargaTiket = $this->input->post("harga_tiket");
			} else {
				$hargaTiket = 0;
			}

			$data = [
				'nama_museum' => $this->input->post("nama_museum"),
				'keterangan' => $this->input->post("keterangan"),
				'alamat' => $this->input->post("alamat"),
				'jadwal_buka_tutup' => $this->input->post("jadwal_buka_tutup"),
				'harga_tiket' => $hargaTiket,
				'id_area' => $this->input->post("area"),
				'id_user'=> $this->session->userdata('login')['id'],
				'slug' => str_slug($this->input->post("nama_museum"))
			];

			if($this->formvalidation->validationRules('museum') && !is_array($gambar)) {

				$data['gambar'] = $gambar;

				if($this->Museum->save($data)) {
					$type = 'success';
					$message = 'Data saved successfully';
				}

			} else {
				$errData = $this->formvalidation->getErrMsg('museum', $data);

				if(is_string($gambar)) {
					unlink("./upload/$gambar");
				} else {
					$errData['gambar'] = $gambar;
				}
			}
		}

		$this->session->set_flashdata('type', $type);
		$this->session->set_flashdata('message', $message);

		if(isset($errData)) {
			$this->session->set_flashdata("errData", (object)$errData);
			return redirect("dashboard/museum/add");
		}

		return redirect("dashboard/museum");
	}

	public function edit($id)
	{
		$this->data["museum"] = $this->Museum->get($id);
		$this->data['area'] = $this->Area->all();
		$this->data['action'] = base_url() . "dashboard/museum/$id/update";

		return view("admin.museum.form", $this->data);
	}

	public function update($id)
	{
		$this->load->library("FormValidation");

		$message = "Failed updating data";
		$type = 'error';

		if(strtoupper($this->input->method()) === "POST") {
			
			$stateGambar = true;

			if($this->input->post("harga_tiket")) {
				$hargaTiket = $this->input->post("harga_tiket");
			} else {
				$hargaTiket = 0;
			}

			$data = [
				'nama_museum' => $this->input->post("nama_museum"),
				'keterangan' => $this->input->post("keterangan"),
				'alamat' => $this->input->post("alamat"),
				'jadwal_buka_tutup' => $this->input->post("jadwal_buka_tutup"),
				'harga_tiket' => $hargaTiket,
				'id_area' => $this->input->post("area"),
				'id_user'=> $this->session->userdata('login')['id'],
				'slug' => str_slug($this->input->post("nama_museum"))
			];

			if(!empty($_FILES["gambar"]['name'])) {
				$gambar = $this->uploadImg();
			}

			if(isset($gambar) && is_array($gambar)) {
				$stateGambar = false;
			}

			if($this->formvalidation->validationRules('museum') && $stateGambar) {
				
				$gambarMuseum = $this->Museum->get($id)->gambar;

				if(isset($gambar)) {
					$data['gambar'] = $gambar;
				} else {
					$data['gambar'] = $gambarMuseum;
				}

				if($this->Museum->update($id, $data)) {
					
					if(!empty($_FILES["gambar"]['name'])) {
						unlink("./upload/$gambarMuseum");
					}

					$type = 'success';
					$message = 'Data updated successfully';
				}

			} else {
				$errData = $this->formvalidation->getErrMsg('museum', $data);

				if(is_string($gambar)) {
					unlink("./upload/$gambar");
				} else {
					$errData['gambar'] = $gambar;
				}
			}
		}

		$this->session->set_flashdata('type', $type);
		$this->session->set_flashdata('message', $message);

		if(isset($errData)) {
			$this->session->set_flashdata("errData", (object)$errData);
			return redirect("dashboard/museum/$id/edit");
		}

		return redirect("dashboard/museum");
	}

	public function destroy($id)
	{
		$data = false;
		$type = 'error';
		$message = "Failed deleting museum";

		if(strtoupper($this->input->method()) === "POST") {

			$museum = $this->Museum->get($id);

			if(isset($id) && !empty($id)) {
				$data = $this->Museum->destroy($id);
			}

			if($data) {
				unlink("./upload/$museum->gambar");
				$type = 'success';
				$message = 'Data deleted successfully';
			}
		}
		
		$this->session->set_flashdata('type', $type);
		$this->session->set_flashdata('message', $message);
		return redirect("dashboard/museum");
	}

	private function uploadImg()
	{
		$config['upload_path'] = './upload/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$config['overwrite'] = true;
		$config['file_ext_tolower'] = true;
		$config['remove_spaces'] = true;
		$config['detect_mime'] = true;
		$config['max_size'] = '3000';
		$config['file_name'] = time() . "_" . $this->session->userdata('login')['id'];

		$this->load->library('upload', $config);

		if($this->upload->do_upload('gambar')) {
			return $this->upload->data('file_name');
		} 
		return [
			'class' => 'is-invalid',
			'message' => $this->upload->display_errors(),
		];
	}
}
