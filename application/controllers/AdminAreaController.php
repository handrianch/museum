<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class AdminAreaController extends CI_Controller
{
	public $data = [];

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Area');
		$this->load->model("Setting");

		$this->middleware->auth();
		$this->middleware->protect("area", "area");

		$this->data['settings'] = $this->Setting->get();
		$this->data['bgColor'] = 'bg-success';
		$this->data['page'] = 'Area';

		getFlashMessage();
	}

	public function index()
	{
		$this->load->library("Paginate");

		$pagination = $this->paginate->makeIt(base_url("dashboard/area"), $this->Area->rowCount());
		
		$this->data['pagination'] = $pagination['pagination'];
		$this->data['start'] = $pagination['start'];
		$this->data['area'] = $this->Area->all($pagination['per_page'], $pagination['page']);

		return view("admin.area.index", $this->data);
	}

	public function create()
	{
		$this->data['action'] = base_url("dashboard/area/save");
		return view("admin.area.form", $this->data);
	}

	public function save()
	{
		$this->load->library("FormValidation");

		$message = "Failed creating data";
		$type = 'error';

		if(strtoupper($this->input->method()) === "POST") {
			
			$data = [
				'nama_area' => $this->input->post('nama_area'),
				'id_user' => $this->session->userdata('login')['id']
			];

			if($this->formvalidation->validationRules('area')) {
				
				$data['slug'] = str_slug($data['nama_area']);

				if($this->Area->save($data)) {
					$type = 'success';
					$message = 'Data saved successfully';
				}

			} else {
				$errData = $this->formvalidation->getErrMsg('area', $data);
			}
		}

		$this->session->set_flashdata('type', $type);
		$this->session->set_flashdata('message', $message);

		if(isset($errData)) {
			$this->session->set_flashdata("errData", (object)$errData);
			return redirect("dashboard/area/add");
		}

		return redirect("dashboard/area");
	}

	public function edit($id)
	{
		$this->data["area"] = $this->Area->get($id);
		$this->data['action'] = base_url() . "dashboard/area/$id/update";

		return view("admin.area.form", $this->data);
	}

	public function update($id)
	{
		$this->load->library("FormValidation");

		$message = "Failed updating data area";
		$type = 'error';

		if(strtoupper($this->input->method()) === "POST") {

			$data = [
				'nama_area' => $this->input->post("nama_area"),
			];

			if($this->formvalidation->validationRules('area')) {
				
				$data['slug'] = str_slug($data['nama_area']);

				if($this->Area->update($id, $data)) {
					$type = 'success';
					$message = 'Data updated successfully';
				}
			} else {
				$errData = $this->formvalidation->getErrMsg('area', $data);
			}
		}

		$this->session->set_flashdata('type', $type);
		$this->session->set_flashdata('message', $message);

		if(isset($errData)) {
			$this->session->set_flashdata("errData", (object)$errData);
			return redirect("dashboard/area/$id/edit");
		}

		return redirect("dashboard/area");
	}

	public function destroy($id)
	{
		$data = false;
		$type = 'error';
		$message = "Failed deleting area";

		if(strtoupper($this->input->method()) === "POST") {
			if(isset($id) && !empty($id)) {
				$data = $this->Area->destroy($id);
			}

			if($data) {
				$type = 'success';
				$message = 'Data deleted successfully';
			}
		}
		
		$this->session->set_flashdata('type', $type);
		$this->session->set_flashdata('message', $message);
		return redirect("dashboard/area");
	}
}