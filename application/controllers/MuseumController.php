<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class MuseumController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model("Setting");
		$this->load->model("Museum");

		$this->data['settings'] = $this->Setting->get();
		$this->data['page'] = 'museum';
	}

	public function index()
	{
		$this->load->library("Paginate");

		$pagination = $this->paginate->makeIt(base_url("museum"), $this->Museum->rowCount(), 9, 2);
		
		$this->data['pagination'] = $pagination['pagination'];
		$this->data['start'] = $pagination['start'];
		$this->data['allMuseum'] = $this->Museum->all($pagination['per_page'], $pagination['page']);

		return view('client.museum', $this->data);
	}

	public function detail($namaMuseum)
	{
		$this->data['museum'] = $this->Museum->get($namaMuseum, "slug");
		return view('client.show', $this->data);	
	}

	public function search($keyword)
	{
		
	}

	public function area($area)
	{
		echo "area page working";
	}
}