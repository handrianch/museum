<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Middleware
{
	private $CI = null;
	private $listMethod = null;
	private $skipMiddleware = false;

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->listMethod = get_class_methods($this->CI);
	}

	public function auth()
	{
		if(!($this->CI->session->userdata("login"))) {
			$this->CI->session->set_flashdata('type', "error");
			$this->CI->session->set_flashdata('message', "Login First!");
			redirect("login");
		}
	}

	public function guest($arr = [])
	{
		$methodActive = $this->CI->router->fetch_method();
		$listMethod = $this->listMethod;

		if(!empty($arr)) {
			array_map(function($val) use ($listMethod, $methodActive) {
				if(in_array($val, $listMethod)) {
					if(strtoupper($val) === strtoupper($methodActive)) {
						$this->skipMiddleware = true;
						$this->auth();
					}
				}
			}, $arr);
		}

		if(!($this->skipMiddleware)) {
			if($this->CI->session->userdata("login")) {
				return redirect("dashboard");
			}
		}
	}
	
	public function protect($model, $page)
	{
		$model = ucfirst($model);
		$this->CI->load->model($model);

		$userSess = $this->CI->session->userdata('login');
		$data = $this->CI->{$model}->get($this->CI->uri->segment(3));
		$methodActive = $this->CI->router->fetch_method();

		$thisClassMethod = get_class_methods($this);
		$funcName = $page . "Protect";

		if(in_array($funcName, $thisClassMethod)) {
			if($this->{$funcName}($methodActive, $userSess, $data)) {
				$this->CI->session->set_flashdata('type', 'error');
				$this->CI->session->set_flashdata('message', 'Access denied');
				redirect("dashboard/$page");
			}
		} else {
			return redirect("dashboard/$page");
		}
	}

	public function usersProtect($methodActive, $userSess, $data)
	{	
		$userId = $data ? $data->id : 0;

		if($methodActive !== 'index') {
			if($userSess['role'] == 1 && $userId != 1) {
				return false;
			} elseif($userSess['id'] != $userId) {
				return true;
			}
		}

		return false;
	}

	public function areaProtect($methodActive, $userSess, $data)
	{
		$userId = $data ? $data->id_user : 0;

		if($this->mainProtect($methodActive, $userSess, $userId)) {
			return true;
		}

		return false;
	}

	public function museumProtect($methodActive, $userSess, $data)
	{
		$userId = $data ? $data->id_user : 0;

		if($this->mainProtect($methodActive, $userSess, $userId)) {
			return true;
		}
		
		return false;
	}

	public function mainProtect($methodActive, $userSess, $id)
	{
		if($methodActive != 'index' && $methodActive != "create") {
			if($methodActive == 'edit' || $methodActive == 'update' || $methodActive == 'destroy') {
				if($userSess['role'] == 1 && $userSess['id'] == 1) {
					return false;
				} elseif($userSess['role'] == 1 && $id != 1) {
					return false;
				}elseif($userSess['id'] != $id) {
					return true;
				}
			}
		}

		return false;
	}
}