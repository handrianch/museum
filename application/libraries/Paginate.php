<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Paginate
{
	private $CI = null;
	private $data = [];

	public function __construct()
	{
		$this->CI =& get_instance();
	}

	public function makeIt($baseURL, $rowCount, $perPage = 8, $segment = 3)
	{
		$this->CI->load->library('pagination');

		$config['base_url'] = $baseURL;
		$config['total_rows'] = $rowCount;
		$config['per_page'] = $perPage;
		$config['uri_segment'] = $segment;
		$totalLinkPagination = $config['total_rows'] / $config['per_page'];
		$config['num_links'] = ceil($totalLinkPagination);

		$config['full_tag_open'] = '<div class="col"><nav class="mt-1"><ul class="pagination justify-content-end">';
		$config['full_tag_close'] = '</ul></nav></div>';
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
		$config['attributes'] = array('class' => 'page-link');
		$config["use_page_numbers"] = true;
		$config["num_links"] = 2;
		$config['next_link'] = false;
		$config['prev_link'] = false;
		$config['next'] = false;

		$this->CI->pagination->initialize($config);
		$this->data['pagination'] = $this->CI->pagination->create_links();

		$page = $this->CI->uri->segment($segment) ? $this->CI->uri->segment($segment) : 0;
		$this->data['start'] = ($page == 0 ? 1 : (($page - 1) * $config["per_page"] + 1));
		
		$this->data['page'] = ($page > 1) ? ($page * $config['per_page']) - $config['per_page'] : 0;
		$this->data['per_page'] = $config['per_page'];
		
		return $this->data;
	}
}