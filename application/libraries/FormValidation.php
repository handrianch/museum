<?php 
defined("BASEPATH") OR exit("No direct script Access allowed");

class FormValidation
{
	private $CI = '';
	private $config = [
		"user" => [
			[
				"field" => 'nama',
				"label" => 'Nama',
				"rules" => 'trim|required|min_length[3]',
				"errors" => [
					'required' => "field %s harus di isi tidak boleh kosong!",
					'min_length' => "field %s di isi minimal 3 karakter"
				],
			],
			[
				"field" => 'email',
				"label" => 'Email',
				"rules" => 'trim|required|valid_email|is_unique[users.email]',
				"errors" => [
					'required' => "field %s harus di isi tidak boleh kosong!",
					'valid_email' => "field %s harus di isi dengan email yang valid",
					'is_unique' => "%s sudah terdaftar"
				]
			],
			[
				"field" => 'password',
				"label" => 'Password',
				"rules" => 'trim|required|min_length[5]',
				"errors" => [
					'required' => "field %s harus di isi tidak boleh kosong!",
					'min_length' => "field %s di isi minimal 5 karakter"
				]
			],
			[
				"field" => 'password_konfirmasi',
				"label" => 'Password Konfimasi',
				"rules" => 'trim|required|matches[password]',
				"errors" => [
					'required' => "field %s harus di isi tidak boleh kosong!",
					'matches' => "%s tidak sesuai",
				]
			]
		],
		'area' => [
			[
				"field" => 'nama_area',
				"label" => 'Nama Area',
				"rules" => 'trim|required|is_unique[area.nama_area]',
				"errors" => [
					"required" => "field %s harus di isi tidak boleh kosong!",
					'is_unique' => "%s sudah ada"
				]
			]
		],
		'auth' => [
			[
				"field" => 'email',
				"label" => 'Email',
				"rules" => 'trim|required|valid_email',
				"errors" => [
					"required" => "field %s harus di isi tidak boleh kosong!",
					'valid_email' => "field %s harus di isi dengan email yang valid",
				]
			],
			[
				"field" => 'password',
				"label" => 'Password',
				"rules" => 'trim|required',
				"errors" => [
					"required" => "field %s harus di isi tidak boleh kosong!",
				]
			]
		],
		'museum' => [
			[
				"field" => 'nama_museum',
				"label" => 'Nama Museum',
				"rules" => 'trim|required',
				"errors" => [
					'required' => "field %s harus di isi tidak boleh kosong!",
				]
			],
			[
				"field" => 'keterangan', 
				"label" => 'Keterangan',
				"rules" => 'trim|required|min_length[17]',
				"errors" => [
					'required' => "field %s harus di isi tidak boleh kosong!",
					'min_length' => "field %s di isi minimal 10 karakter"
				]
			],
			[
				"field" => 'alamat', 
				"label" => 'Alamat',
				"rules" => 'trim|required|min_length[12]',
				"errors" => [
					'required' => "field %s harus di isi tidak boleh kosong!",
					'min_length' => "field %s di isi minimal 10 karakter"
				]
			],
			[
				"field" => 'jadwal_buka_tutup', 
				"label" => 'Jadwal Buka Tutup',
				"rules" => 'trim|required',
				"errors" => [
					'required' => "field %s harus di isi tidak boleh kosong!",
				]
			],
			[
				"field" => 'area', 
				"label" => 'Area',
				"rules" => 'required|alpha_numeric',
				"errors" => [
					'required' => "field %s harus di isi tidak boleh kosong!",
				]
			],
		],
		'settings' => [
			[
				"field" => 'nama_website',
				"label" => 'Nama Website',
				"rules" => 'trim|required',
				"errors" => [
					'required' => "field %s harus di isi tidak boleh kosong!",
				]
			],
			[
				"field" => 'home',
				"label" => 'Home Text',
				"rules" => 'trim|required',
				"errors" => [
					'required' => "field %s harus di isi tidak boleh kosong!",
				]
			],
			[
				"field" => 'about',
				"label" => 'About Text',
				"rules" => 'trim|required',
				"errors" => [
					'required' => "field %s harus di isi tidak boleh kosong!",
				]
			],
			[
				"field" => 'contact',
				"label" => 'Contact Text',
				"rules" => 'trim|required',
				"errors" => [
					'required' => "field %s harus di isi tidak boleh kosong!",
				]
			],
		],
	];

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library("form_validation");
	}

	public function validationRules($conf, $email = true, $password = true)
	{
		if(!$password) {
			array_splice($this->config[$conf] , 2, 2);
		}
		
		if(strtoupper($conf) === 'USER' && !$email) {
			$this->config[$conf][1]['rules'] = 'trim|required|valid_email'; 
		}

		$this->CI->form_validation->set_rules($this->config[$conf]);

		if($this->CI->form_validation->run()) {
			return true;
		}

		return false;
	}

	public function getErrMsg($module, $data)
	{
		if($module == 'user') {
			$errData = [
				'user' => [
					'nama' => [
						'class' => !empty(form_error('nama')) ? 'is-invalid' : '',
						'message' => form_error('nama'),
						'value' => $data['nama'] ?: '', 
					],
					'email' => [
						'class' => !empty(form_error('email')) ? 'is-invalid' : '',
						'message' => form_error('email'),
						'value' => $data['email'] ?: '',
					],
					'password' => [
						'class' => !empty(form_error('password')) ? 'is-invalid' : '',
						'message' => form_error('password')
					],
					'password_konfirmasi' => [
						'class' => !empty(form_error('password_konfirmasi')) ? 'is-invalid' : '',
						'message' => form_error('password_konfirmasi')
					]
				]
			];
		} elseif($module == 'auth') {
			$errData = [
				'auth' => [
					'email' => [
						'class' => !empty(form_error('email')) ? 'is-invalid' : '',
						'message' => form_error('email'),
						'value' => $data['email'],
					],
					'password' => [
						'class' => !empty(form_error('password')) ? 'is-invalid' : '',
						'message' => form_error('password'),
					]
				]
			];
		} elseif($module == 'area') {
			$errData = [
				'area' => [
					'nama_area' => [
						'class' => !empty(form_error('nama_area')) ? 'is-invalid' : '',
						'message' => form_error('nama_area'),
						'value' => $data['nama_area'] ?: '',
					]
				],
			];
		} elseif($module == 'museum') {
			$errData = [
				'museum' => [
					'nama_museum' => [
						'class' => !empty(form_error('nama_museum')) ? 'is-invalid' : '',
						'message' => form_error('nama_museum'),
						'value' => $data['nama_museum'] ?: '',
					],
					'keterangan' => [
						'class' => !empty(form_error('keterangan')) ? 'is-invalid' : '',
						'message' => form_error('keterangan'),
						'value' => $data['keterangan'] ?: '',
					],
					'alamat' => [
						'class' => !empty(form_error('alamat')) ? 'is-invalid' : '',
						'message' => form_error('alamat'),
						'value' => $data['alamat'] ?: '',
					],
					'jadwal_buka_tutup' => [
						'class' => !empty(form_error('jadwal_buka_tutup')) ? 'is-invalid' : '',
						'message' => form_error('jadwal_buka_tutup'),
						'value' => $data['jadwal_buka_tutup'] ?: '',
					],
					'area' => [
						'class' => !empty(form_error('area')) ? 'is-invalid' : '',
						'message' => form_error('area'),
						'value' => $data['id_area'] ?: '',
					],
					'harga_tiket' => [
						'value' => $data['harga_tiket'] ?: '',
					],
				],
			];
		} elseif($module == 'settings') {
			$errData = [
				'settings' => [
					'nama_website' => [
						'class' => !empty(form_error('nama_website')) ? 'is-invalid' : '',
						'message' => form_error('nama_website'),
						'value' => $data['nama_website'] ?: '',
					],
					'home' => [
						'class' => !empty(form_error('home')) ? 'is-invalid' : '',
						'message' => form_error('home'),
						'value' => $data['home'] ?: '',
					],
					'about' => [
						'class' => !empty(form_error('about')) ? 'is-invalid' : '',
						'message' => form_error('about'),
						'value' => $data['about'] ?: '',
					],
					'contact' => [
						'class' => !empty(form_error('contact')) ? 'is-invalid' : '',
						'message' => form_error('contact'),
						'value' => $data['contact'] ?: '',
					],
				],
			];
		}

		return $errData[$module];
	}
}