<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Auth
{
	private $CI = null;
	private $data = null;

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model("User");
	}

	public function attempt($data)
	{
		$this->data = $data;
		$user = $this->CI->User->get($data['email'], 'email');

		if(!empty($user)) {
			if(password_verify($data['password'], $user->password)) {
				$this->CI->session->set_flashdata('type', 'success');
				$this->CI->session->set_flashdata('message', 'Login successfully');
				$sesUserData = [
					login => [
						'id' => $user->id,
						'nama' => $user->nama,
						'role' => $user->role_id,
					]
				];
				$this->CI->session->set_userdata($sesUserData);
				return true;
			}
		}
		
		return false;
	}

	public function logout()
	{
		if(strtoupper($this->CI->input->method()) === "POST") {
			$this->CI->session->sess_destroy();
			return true;
		}

		return false;
	}

	public function getErrMsg()
	{
		return [
			'email' => [ 
				'value' => $this->data['email'] ,
				'message' => "Login failed, check again your credentials",
			]
		];
	}
}