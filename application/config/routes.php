<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

// Home Frontend Route
$route['default_controller'] = 'HomeController';
$route['tentang'] = 'HomeController/about';
$route['kontak'] = 'HomeController/contact';

// Museum Frontend Route
$route['museum'] = 'MuseumController/index';
$route['museum/(:any)'] = 'MuseumController/index';
$route['museum/(:any)/show'] = 'MuseumController/detail/$1';
$route['museum/search/(:any)'] = 'MuseumController/search/$1';
$route['museum/wilayah/(:any)'] = 'MuseumController/area/$1';

// Dashboard Backend Route
$route['dashboard'] = 'DashboardController/index';

// Settings Web Backend Route
$route['dashboard/settings'] = 'SettingsController/index';
$route['dashboard/settings/save'] = 'SettingsController/config';

// User Backend Route
$route['dashboard/users'] = 'AdminUserController/index';
$route['dashboard/users/(:num)'] = 'AdminUserController/index';
$route['dashboard/users/add'] = 'AdminUserController/create';
$route['dashboard/users/save'] = 'AdminUserController/save'; 
$route['dashboard/users/(:num)/edit'] = 'AdminUserController/edit/$1'; 
$route['dashboard/users/(:num)/update'] = 'AdminUserController/update/$1'; 
$route['dashboard/users/(:num)/delete'] = 'AdminUserController/delete/$1'; 

// Museum Backend Route
$route['dashboard/museum'] = 'AdminMuseumController/index';
$route['dashboard/museum/(:num)'] = 'AdminMuseumController/index';
$route['dashboard/museum/add'] = 'AdminMuseumController/create';
$route['dashboard/museum/save'] = 'AdminMuseumController/save'; 
$route['dashboard/museum/(:num)/edit'] = 'AdminMuseumController/edit/$1'; 
$route['dashboard/museum/(:num)/update'] = 'AdminMuseumController/update/$1'; 
$route['dashboard/museum/(:num)/delete'] = 'AdminMuseumController/destroy/$1'; 

// Area Backend Route
$route['dashboard/area'] = 'AdminAreaController/index';
$route['dashboard/area/(:num)'] = 'AdminAreaController/index';
$route['dashboard/area/add'] = 'AdminAreaController/create';
$route['dashboard/area/save'] = 'AdminAreaController/save';
$route['dashboard/area/(:num)/edit'] = 'AdminAreaController/edit/$1';
$route['dashboard/area/(:num)/update'] = 'AdminAreaController/update/$1';
$route['dashboard/area/(:num)/delete'] = 'AdminAreaController/destroy/$1';

// Login & Logout Routes
$route['login'] = 'AuthenticationController/index';
$route['attempt'] = 'AuthenticationController/login';
$route['dashboard/logout'] = 'AuthenticationController/logout';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
