@extends("admin.master.master")

@section("content")
	<div class="container">
		<div class="row pb-4">
			<div class="col">
				<h4 class="display-4">
					@isset($users)
					 &gt;{{ "Edit User" }} <strong class="text-underline font-italic"><u>{{ ucfirst($users->nama) }}</u></strong>
					@else
						&#43;{{ "Add User" }}
					@endisset
				</h4>
			</div>
		</div>
		<form method="POST" action="{{ $action }}">
			<div class="row pb-3">
				<div class="col">
					<div class="form-group">
						<label for="nama">Nama</label>
						<input type="text" name="nama" 
						@if(!empty($errData->nama['value']))
							value="{{ $errData->nama['value'] }}" 
						@elseif(isset($users->nama))
							value="{{ $users->nama }}"
						@endif
						class="form-control {{ (!empty($errData->nama['class']) ? $errData->nama['class'] : '') }}">
						
            @isset($errData->nama['message'])
							<div class="invalid-feedback">
								{!! $errData->nama['message'] !!}
							</div>
						@endisset
					</div>		
				</div>
				
				<div class="col">
					<div class="form-group">
						<label for="email">Email</label>
						<input type="email" name="email" 
							@if(!empty($errData->email['value']))
								value="{{ $errData->email['value'] }}" 
							@elseif(isset($users->email))
								value="{{ $users->email }}"
							@endif
							class="form-control {{ !empty($errData->email['class']) ? $errData->email['class'] : "" }}">
						  
              @isset($errData)
  							<div class="invalid-feedback">
  								{!! $errData->email['message'] !!}
  							</div>
  						@endisset
					</div>		
				</div>
			</div>

			<div class="row pb-3">
				<div class="col">
					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" name="password" 
							class="form-control {{ !empty($errData->password['class']) ? $errData->password['class'] : '' }}">
						
            @isset($errData)
  						<div class="invalid-feedback">
  							{!! $errData->password['message'] !!}
  						</div>
						@endisset
					</div>		
				</div>

				<div class="col">
					<div class="form-group">
						<label for="password_konfirmasi">Password konfirmasi</label>
						<input type="password" name="password_konfirmasi" 
						  class="form-control {{ !empty($errData->password_konfirmasi['class']) ? $errData->password_konfirmasi['class'] : '' }}">
						
            @isset($errData)
							<div class="invalid-feedback">
								{!! $errData->password_konfirmasi['message'] !!}
							</div>
						@endisset
					</div>		
				</div>
			</div>
      
			@if($login['role'] == 1 && $login['id'] == 1)
				<div class="row pb-3">
					<div class="col">
						@foreach($roleList as $role)
							<div class="form-group">
								<input type="radio" name="role" value="{{ $role->id }}" 
								@isset($users->role_id)
								  {{ ($role->id == $users->role_id) ? 'checked' : ''}}>
								@else
									{{ ((int)$role->id === (int)2) ? 'checked' : '' }}>
								@endisset
								{{ $role->role_name }}
							</div>
						@endforeach
					</div>
				</div>
			@endif

			<div class="row align-items-center">
				<div class="col">
					<div class="form-group">
						<a href="{{ base_url("dashboard/users") }}" class="btn btn-danger btn-lg btn-block">
							<i class="fa fa-close"></i> Cancel
						</a>
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<button class="btn btn-lg btn-block btn-warning"><i class="fa fa-save"></i> Save</button>
					</div>
				</div>
			</div>
		</form>
	</div>
@endsection


@if(isset($type) && isset($message))
  @section('script')
  	<script type="text/javascript">
  		var type = "{{ $type }}";
  		var message = "{{ $message }}"
  	</script>
    <script type="text/javascript" src="{{ base_url("assets/backend/js/toast.js")}}"></script>
  @endsection
@endif