@extends("admin.master.master")

@section("content")
  <section id="users" class="full-height">
    <div class="container">
      <div class="row">
        <div class="col">
          @if(count($users) <= 0)
            <div class="text-center">
              <h5 class="display-1 mb-5">Empty Data</h5>
              <a href="{{ base_url("dashboard/users") }}" class="btn btn-lg btn-warning">
                <i class="fa fa-arrow-circle-left"></i> Go Back To The Earth
              </a>
            </div>
          @else
            <div class="card">
                <div class="card-header">
                    <h4>Users</h4>
                </div>
                
                <table class="table table-striped">
                    <thead class="thead-inverse">
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Email</th>
                            @if($login['role'] == 1)
                                <th>Action</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr data-set-id="{{ $user->id }}">
                                <td scope="row">{{ $start }} </td>
                                <td>{{ $user->nama }}</td>
                                <td>{{ $user->email }}</td>
                                @if($login['role'] == 1)
                                    <td>
                                        @if($user->id != 1 || $login['id'] == 1)
                                            <a href="{{ base_url("dashboard/users/{$user->id}/edit") }}"
                                               class="btn btn-secondary"><i class="fa fa-angle-double-right"></i> Edit
                                            </a>
                                        @endif
                                        @if($user->id != $login['id'] && $user->id != 1)
                                            <button class="btn btn-danger delete-btn"><i class="fa fa-trash"></i> Delete</button>
                                        @endif
                                    </td>
                                @endif
                            </tr>
                            @php
                                $start++;
                            @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
          @endif
        </div>
      </div>
      
      @if(count($users) > 0)
        <div class="row">
            {!! $pagination !!}
        </div>
      @endif
    </div>
  </section>

  <form class="d-none" method="POST" id="set-delete">
  </form>
@endsection

@section('script')
  @if(isset($type) && isset($message))
    <script type="text/javascript">
      var type = "{{ $type }}";
      var message = "{{ $message }}"
    </script>
    <script type="text/javascript" src="{{ base_url("assets/backend/js/toast.js")}}"></script>
  @endif

  <script type="text/javascript">
    var baseURL = "{{ base_url("dashboard/users") }}";
  </script>
  <script type="text/javascript" src="{{ base_url("assets/backend/js/popup.js")}}"></script>
@endsection