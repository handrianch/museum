@extends("admin.master.master")

@section("content")
	<section id="area" class="full-height">
    <div class="container">
      <div class="row">
        <div class="col">
          @if(count($area) <= 0)
            <div class="text-center">
              <h5 class="display-1 mb-5">Empty Data</h5>
              <a href="{{ base_url("dashboard/area") }}" class="btn btn-lg btn-success">
                <i class="fa fa-arrow-circle-left"></i> Go Back To The Earth
              </a>
						</div>
        	@else
            <div class="card">
              <div class="card-header">
                <h4>Area</h4>
              </div>
                
              <table class="table table-striped">
                <thead class="thead-inverse">
                  <tr>
                    <th>No</th>
                    <th>Area Name</th>
                    <th>Created By</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($area as $district)
                    <tr data-set-id="{{ $district->id }}">
                      <td scope="row">{{ $start }} </td>
                      <td>{{ $district->nama_area }}</td>
                      <td>{{ $district->nama }}</td>
                      <td>
                        @if($district->id_user == $login['id'] || $login['id'] == 1 || $login['role'] == 1)
                          <a href="{{ base_url("dashboard/area/{$district->id}/edit") }}" class="btn btn-secondary">
                            <i class="fa fa-angle-double-right"></i> Edit
                          </a>
                          <button class="btn btn-danger delete-btn"><i class="fa fa-trash"></i> Delete</button>
                        @endif
                      </td>
                    </tr>
                    @php
                        $start++;
                    @endphp
                  @endforeach
                </tbody>
              </table>
            </div>
          @endif
				</div>
      </div>
      @if(count($area) > 0)
        <div class="row">
            {!! $pagination !!}
        </div>
      @endif
    </div>
	</section>

  <form class="d-none" method="POST" id="set-delete">
  </form>
@endsection

@section('script')
	@if(isset($type) && isset($message))
		<script type="text/javascript">
			var type = "{{ $type }}";
			var message = "{{ $message }}";
		</script>
		<script type="text/javascript" src="{{ base_url("assets/backend/js/toast.js")}}"></script>
	@endif
		<script type="text/javascript">
			var baseURL = "{{ base_url("dashboard/area") }}";
		</script>
		<script type="text/javascript" src="{{ base_url("assets/backend/js/popup.js")}}"></script>
@endsection