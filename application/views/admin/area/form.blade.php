@extends("admin.master.master")

@section("content")
	<div class="container">
		<div class="row pb-4">
			<div class="col">
				<h4 class="display-4">
					@isset($area)
						&gt;{{ "Edit Area" }} <strong class="text-underline font-italic"><u>{{ ucfirst($area->nama_area) }}</u></strong>
					@else
						&#43;{{ "Add Area" }}
					@endisset
				</h4>
			</div>
		</div>
		<form method="POST" action="{{ $action }}">
			<div class="row pb-3">
				<div class="col">
					<div class="form-group">
						<label for="nama">Nama Area</label>
						<input type="text" name="nama_area" 
							@if(!empty($errData->nama_area['value']))
								value="{{ $errData->nama_area['value'] }}" 
							@elseif(isset($area->nama_area))
								value="{{ $area->nama_area }}"
							@endif
							class="form-control {{ (!empty($errData->nama_area['class']) ? $errData->nama_area['class'] : '') }}">
						@isset($errData->nama_area['message'])
							<div class="invalid-feedback">
								{!! $errData->nama_area['message'] !!}
							</div>
						@endisset
					</div>
				</div>
			</div>

			<div class="row align-items-center">
				<div class="col">
					<div class="form-group">
						<a href="{{ base_url("dashboard/area") }}" class="btn btn-danger btn-lg btn-block">
							<i class="fa fa-close"></i> Cancel</a>
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<button class="btn btn-lg btn-block btn-success"><i class="fa fa-save"></i> Save</button>
					</div>
				</div>
			</div>
		</form>
	</div>
@endsection


@if(isset($type) && isset($message))
	@section('script')
		<script type="text/javascript">
			var type = "{{ $type }}";
			var message = "{{ $message }}"
		</script>
		<script type="text/javascript" src="{{ base_url("assets/backend/js/toast.js")}}"></script>
	@endsection
@endif