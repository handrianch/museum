@extends("admin.master.master")

@section("content")
  <div class="container">
    <div class="row pb-4">
      <div class="col">
        <h4 class="display-4">Web Settings - Last Edited By 
          <small class="text-muted"><u><em>{{ isset($settings->nama) ? $settings->nama : ''}}</em></u></small></h4>
      </div>
    </div>
    <form method="POST" action="{{ $action }}">
      <div class="row pb-3">
        <div class="col">
          <div class="form-group">
            <label for="nama_website">Nama Website</label>
            <input type="text" name="nama_website" id="nama_website"
            @if(!empty($errData->nama_website['value']))
              value="{{ $errData->nama_website['value'] }}" 
            @elseif(isset($settings->nama_website))
              value="{{ $settings->nama_website }}"
            @endif
            class="form-control {{ (!empty($errData->nama_website['class']) ? $errData->nama_website['class'] : '') }}">
            @isset($errData->nama_website['message'])
              <div class="invalid-feedback">
                {!! $errData->nama_website['message'] !!}
              </div>
            @endisset
          </div>    
        </div>
      </div>

      <div class="row pb-3">
        <div class="col">
          <div class="form-group">
            <label for="home">Home Text</label>
            <textarea name="home" id="home" class="form-control {{ (!empty($errData->home['class']) ? $errData->home['class'] : '') }}">
              @php
                if(!empty($errData->home['value'])) {
                  echo $errData->home['value'];
                } elseif(isset($settings->home_text)) {
                   echo $settings->home_text;
                }
              @endphp
            </textarea>
            @isset($errData->home['message'])
              <div class="invalid-feedback">
                {!! $errData->home['message'] !!}
              </div>
            @endisset
          </div>    
        </div>
      </div>

      <div class="row pb-3">
        <div class="col">
          <div class="form-group">
            <label for="about">About Text</label>
            <textarea name="about" id="about" class="form-control {{ (!empty($errData->about['class']) ? $errData->about['class'] : '') }}">
              @php
                if(!empty($errData->about['value'])) {
                  echo $errData->about['value'];
                } elseif(isset($settings->about_text)) {
                   echo $settings->about_text;
                }
              @endphp
            </textarea>
            @isset($errData->about['message'])
              <div class="invalid-feedback">
                {!! $errData->about['message'] !!}
              </div>
            @endisset
          </div>    
        </div>
      </div>

      <div class="row pb-3">
        <div class="col">
          <div class="form-group">
            <label for="contact">Contact Text</label>
            <textarea name="contact" id="contact" class="form-control {{ (!empty($errData->contact['class']) ? $errData->contact['class'] : '') }}">
              @php
              if(!empty($errData->contact['value'])) {
                echo $errData->contact['value'];
              } elseif(isset($settings->contact_text)) {
                 echo $settings->contact_text;
              }
            @endphp
          </textarea>
            @isset($errData->contact['message'])
              <div class="invalid-feedback">
                {!! $errData->contact['message'] !!}
              </div>
            @endisset
          </div>    
        </div>
      </div>

      <div class="row align-items-center ">
        <div class="col">
          <div class="form-group">
            <a href="{{ base_url("dashboard/museum") }}" class="btn btn-danger btn-lg btn-block">
              <i class="fa fa-close"></i> Cancel
            </a>
          </div>
        </div>

        <div class="col">
          <div class="form-group">
            <button class="btn btn-lg btn-block btn-primary"><i class="fa fa-save"></i> Save</button>
          </div>
        </div>
      </div>
    </form>
  </div>
@endsection


@section('script')
  @if(isset($type) && isset($message))
    <script type="text/javascript">
      var type = "{{ $type }}";
      var message = "{{ $message }}"
    </script>
    <script type="text/javascript" src="{{ base_url("assets/backend/js/toast.js")}}"></script>
  @endif
  <script type="text/javascript" src="{{ base_url("assets/backend/js/settings.js")}}"></script>
@endsection