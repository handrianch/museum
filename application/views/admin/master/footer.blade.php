<footer id="main-footer" class="bg-dark text-white mt-5 p-3 footer">
  <div class="container">
    <div class="row">
      <div class="col">
        <p class="text-center pt-3">
          Copyright <i class="fa fa-copyright"></i> 2018 
          <span>Made with <i class="fa fa-heart text-danger"></i> &amp; pain</span>
        </p>
      </div>
    </div>
  </div>
</footer>
