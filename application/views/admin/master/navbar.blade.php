<!-- NAVBAR -->
<nav class="navbar navbar-expand-sm navbar-dark bg-dark p-0">
  <div class="container">
    <a href="{{ base_url() }}" class="navbar-brand my-navbar-brand">{{ $settings->nama_website }}</a>
    <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item px-2">
          <a href="{{ base_url("dashboard") }}" class="nav-link 
            {{ (strtoupper($page) == "DASHBOARD") ? "active" : "" }}">
            Dashboard
          </a>
        </li>
      
        <li class="nav-item px-2">
          <a href="{{ base_url("dashboard/museum") }}" class="nav-link 
            {{ (strtoupper($page) == "MUSEUM") ? "active" : "" }}">
              Museum
          </a>
        </li>
      
        <li class="nav-item px-2">
          <a href="{{ base_url("dashboard/area") }}" class="nav-link 
             {{ (strtoupper($page) == "AREA") ? "active" : "" }}">
            Area
          </a>
        </li>
      
        <li class="nav-item px-2">
          <a href="{{ base_url("dashboard/users") }}" class="nav-link 
             {{ (strtoupper($page) == "USERS") ? "active" : "" }}">
            Users
          </a>
      </li>
      </ul>

      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown mr-3">
          <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-user"></i> {{ $login['nama'] }}
          </a>

          <div class="dropdown-menu">
            <a href="{{ base_url("dashboard/users/$login[id]/edit") }}" class="dropdown-item">
                <i class="fa fa-user"></i> Profile Setting
            </a>

            @if(strtoupper($login['role']) == 1)
              <a href="{{ base_url("dashboard/settings") }}" class="dropdown-item">
                  <i class="fa fa-gear"></i> Web Setting
              </a>
            @endif

            <form action="{{ base_url("dashboard/logout") }}" method="POST">
              <button class="dropdown-item"><i class="fa fa-user-times"></i> Logout</button>
            </form>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>