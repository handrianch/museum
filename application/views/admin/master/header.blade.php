<!-- HEADER -->
<header id="main-header" class="py-2 {{ $bgColor }} text-white">
	<div class="container">
		<div class="row">
		  <div class="com-md-6 pl-1">
	      <h1><i class="fa fa-cogs"></i> {{ $page }}</h1>
		  </div>
		</div>
	</div>
</header>