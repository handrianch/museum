<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Museum Dashboard</title>
    <link rel="stylesheet" href="{{ base_url("assets/backend/css/vendor/bootstrap.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ base_url("assets/backend/css/vendor/font-awesome.min.css") }}">
    <link rel="stylesheet" href="{{ base_url("assets/backend/css/vendor/circle-loading.css") }}">
    <link rel="stylesheet" href="{{ base_url("assets/backend/css/vendor/sweetalert2.min.css") }}">
    <link rel="stylesheet" href="{{ base_url("assets/backend/css/main.css") }}">
    <link rel="stylesheet" href="{{ base_url("assets/backend/css/sticky-footer.css") }}">
    @yield("styling")
  </head>
  <body>
    @include("admin.master.navbar")

    @include("admin.master.header")

    @include("admin.master.action")

    @yield("content")

    @include("admin.master.footer")

    <script src="{{ base_url("assets/backend/js/vendor/jquery.min.js") }}"></script>
    <script src="{{ base_url("assets/backend/js/vendor/bootstrap.bundle.min.js") }}"></script>
    <script src="{{ base_url("assets/backend/js/vendor/ckeditor/ckeditor.js") }}"></script>
    <script src="{{ base_url("assets/backend/js/vendor/sweetalert2.all.min.js") }}"></script>
    @yield("script")
  </body>
</html>
