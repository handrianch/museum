<!-- ACTIONS -->
<section id="action" class="py-4 mb-4 bg-light">
  <div class="container">
    <div class="row {{ strtoupper($page) == "DASHBOARD" ? "justify-content-center" : "justify-content-start" }} ">
      @if(strtoupper($page) == "DASHBOARD" || strtoupper($page) == "MUSEUM")
        <div class="col-md-3 mb-2">
          <a href="<?= base_url("dashboard/museum/add"); ?>" class="btn btn-primary btn-block">
              <i class="fa fa-plus"></i> Add Museum
          </a>
        </div>
      @endif

      @if(strtoupper($page) == "DASHBOARD" || strtoupper($page) == "AREA")
        <div class="col-md-3 mb-2">
          <a href="<?= base_url("dashboard/area/add"); ?>" class="btn btn-success btn-block">
              <i class="fa fa-plus"></i> Add Area
          </a>
        </div>
      @endif
            
      @if($login['role'] == 1)
        @if(strtoupper($page) == "DASHBOARD" || strtoupper($page) == "USERS")
          <div class="col-md-3">
            <a href="<?= base_url("dashboard/users/add"); ?>" class="btn btn-warning btn-block">
                <i class="fa fa-plus"></i> Add User
            </a>
          </div>
        @endif
      @endif
    </div>
  </div>
</section>
