<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Admin Area Login</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/png" href="{{ base_url("assets/backend/img/icons/favicon.ico") }}"/>
		<link rel="stylesheet" type="text/css" href="{{ base_url("assets/backend/css/vendor/bootstrap.min.css")}} ">
		<link rel="stylesheet" type="text/css" href="{{ base_url("assets/backend/css/vendor/font-awesome.min.css") }}">
		<link rel="stylesheet" type="text/css" href="{{ base_url("assets/backend/css/util.css") }}">
		<link rel="stylesheet" type="text/css" href="{{ base_url("assets/backend/css/login.css")}} ">
	</head>
	
	<body>
		
		<div class="limiter">
			<div class="container-login100">
				<div class="wrap-login100 p-t-50 p-b-90">
					<form class="login100-form validate-form flex-sb flex-w" action="{{ $action }}" method="POSt">
						<span class="login100-form-title p-b-51"> Login </span>

						@isset($errData->email['message'])
							<div class="invalid-feedback" style="display: block">
								{!! $errData->email['message'] !!}
							</div>
						@endisset
						<div class="wrap-input100 validate-input m-b-16" data-validate = "Email is required">
							<input class="input100 {{ (!empty($errData->email['class']) ? $errData->email['class'] : '') }}" 
									type="text" name="email"
								@isset($errData)
									value="{{ $errData->email['value'] }}"
								@endisset
								placeholder="Email" autocomplete="off">
							<span class="focus-input100"></span>
						</div>
					
						@isset($errData->password['message'])
							<div class="invalid-feedback" style="display: block">
								{!! $errData->password['message'] !!}
							</div>
						@endisset
						
						<div class="wrap-input100 validate-input m-b-16 " data-validate = "Password is required">
							<input class="input100 {{ (!empty($errData->email['class']) ? $errData->email['class'] : '') }}" 
									type="password" name="password" placeholder="Password">
							<span class="focus-input100"></span>
						</div>

						<div class="container-login100-form-btn m-t-17">
							<button class="login100-form-btn">Login</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	
		<script src="{{ base_url("assets/backend/js/vendor/jquery.min.js") }}"></script>
		<script src="{{ base_url("assets/backend/js/vendor/bootstrap.bundle.min.js") }}"></script>
		<script src="{{ base_url("assets/backend/js/vendor/sweetalert2.all.min.js") }}"></script>
		<script src="{{ base_url("assets/backend/js/login.js") }}"></script>
		@if(isset($type) && isset($message))
	    <script type="text/javascript">
				(function() {
					swal({
						type: "{{ $type }}",
						title: 'Oops...',
						text: "{{ $message }}",
					});
				})();
	    </script>
		@endif
	</body>
</html>