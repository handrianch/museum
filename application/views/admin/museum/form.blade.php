@extends("admin.master.master")

@section("content")
	<div class="container">
		<div class="row pb-4">
			<div class="col">
				<h4 class="display-4">
					@isset($museum)
            &gt;{{ "Edit Museum" }} <strong class="text-underline font-italic"><u>{{ ucfirst($museum->nama_museum) }}</u></strong>
					@else 	
						&#43;{{ "Add Museum" }}
					@endisset
				</h4>
			</div>
		</div>
		<form method="POST" action="{{ $action }}" enctype="multipart/form-data">
			<div class="row pb-3">
				<div class="col">
					<div class="form-group">
						<label for="nama_museum">Nama Museum</label>
						<input type="text" name="nama_museum" id="nama_museum"
						@if(!empty($errData->nama_museum['value']))
							value="{{ $errData->nama_museum['value'] }}" 
						@elseif(isset($museum->nama_museum))
							value="{{ $museum->nama_museum }}"
						@endif
						class="form-control {{ (!empty($errData->nama_museum['class']) ? $errData->nama_museum['class'] : '') }}">
						@isset($errData->nama_museum['message'])
							<div class="invalid-feedback">
								{!! $errData->nama_museum['message'] !!}
							</div>
						@endisset
					</div>		
				</div>
			</div>

			<div class="row pb-3">
				<div class="col">
					<div class="form-group">
						<label for="keterangan">Keterangan</label>
						<textarea name="keterangan" id="keterangan" class="form-control 
							{{ (!empty($errData->keterangan['class']) ? $errData->keterangan['class'] : '') }}">
              @php
  							if(!empty($errData->keterangan['value'])) {
  								echo $errData->keterangan['value'];
  							} elseif(isset($museum->keterangan)) {
  								 echo $museum->keterangan;
  							}
						  @endphp
            </textarea>
						@isset($errData->keterangan['message'])
							<div class="invalid-feedback">
								{!! $errData->keterangan['message'] !!}
							</div>
						@endisset
					</div>		
				</div>
			</div>

			<div class="row pb-3">
				<div class="col">
					<div class="form-group">
						<label for="Alamat">Alamat</label>
						<textarea name="alamat" id="alamat" class="form-control
						  {{ (!empty($errData->alamat['class']) ? $errData->alamat['class'] : '') }}">
              @php
  							if(!empty($errData->alamat['value'])) {
  								echo $errData->alamat['value'];
  							} elseif(isset($museum->alamat)) {
  								 echo $museum->alamat;
  							}
  						@endphp
            </textarea>
						@isset($errData->alamat['message'])
							<div class="invalid-feedback">
								{!! $errData->alamat['message'] !!}
							</div>
						@endisset
					</div>		
				</div>

				<div class="col">
					<div class="form-group">
						<label for="jadwal-buka-tutup">Jadwal Buka - Tutup</label>
						<textarea name="jadwal_buka_tutup" class="form-control
						{{ (!empty($errData->jadwal_buka_tutup['class']) ? $errData->jadwal_buka_tutup['class'] : '') }}"
						id="jadwal-buka-tutup">@php
							if(!empty($errData->jadwal_buka_tutup['value'])) {
								echo $errData->jadwal_buka_tutup['value'];
							} elseif(isset($museum->jadwal_buka_tutup)) {
								 echo $museum->jadwal_buka_tutup;
							}
						@endphp</textarea>
						@isset($errData->jadwal_buka_tutup['message'])
							<div class="invalid-feedback">
								{!! $errData->jadwal_buka_tutup['message'] !!}
							</div>
						@endisset
					</div>		
				</div>
			</div>

			<div class="row pb-3">
				<div class="col">
					<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Area</label>
  					</div>
	  				<select name="area" id="inputGroupSelect01" 
              class="{{ (!empty($errData->area['class']) ? $errData->area['class'] : '') }} custom-select">
                <option value="">Choose...</option>
	    					@foreach($area as $district)
                  <option
	    							@isset(($errData->area['value']))
	    								@if($district->id == $errData->area['value'])
	    									{{ 'selected' }}
	    								@endif
	    							@endisset
	    							@isset($museum->id_area)
		    							@if($district->id == $museum->id_area)
		    								{{ 'selected' }}
		    							@endif
		    						@endisset
                    value="{{ $district->id }}">
                      {{ $district->nama_area }}
	    						</option>
	    					@endforeach
	  				</select>
  					@isset($errData->area['message'])
  						<div class="invalid-feedback d-block">
  							{!! $errData->area['message'] !!}
  						</div>
            @endisset
					</div>
				</div>
			</div>
      
      <div class="row pb-3">
        <div class="col-12">
          <p class="text-muted" id="img-title"></p>
          @isset($museum->gambar)
            <img src="{{ base_url("upload/$museum->gambar") }}" id="gambar-thumb" width="200px" class="img-thumbnail rounded" />
          @else
            <img src="" id="gambar-thumb" width="200px" class="img-thumbnail rounded" />
          @endif
        </div>
      </div>

			<div class="row pb-3">
				<div class="col">
					<label>Upload Gambar</label>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
  						<span class="input-group-text">Upload</span>
						</div>
						<div class="custom-file">
  						<input type="file" name="gambar" 
  						class="{{ (!empty($errData->gambar['class']) ? $errData->gambar['class'] : '') }} custom-file-input" 
  						id="gambar" aria-describedby="gambar">
  						<label class="custom-file-label" for="gambar" id="judul-gambar">Choose file</label>
						</div>
						@isset($errData->gambar['message'])
  						<div class="invalid-feedback d-block">
  							{!! $errData->gambar['message'] !!}
  						</div>
					 @endisset
					</div>
				</div>

				<div class="col">
					<div class="form-group">
						<label for="harga-tiket">Harga Tiket</label>
            <textarea name="harga_tiket" id="harga-tiket" placeholder="Jika tidak di isi maka harga tiket gratis" 
              class="form-control 
              {{ (!empty($errData->harga_tiket['class']) ? $errData->harga_tiket['class'] : '') }}">@php
                if(!empty($errData->harga_tiket['value'])) {
                  echo $errData->harga_tiket['value'];
                } elseif(isset($museum->harga_tiket)) {
                   echo $museum->harga_tiket;
                }
              @endphp
            </textarea>
					</div>		
				</div>
			</div>

			<div class="row align-items-center ">
        <div class="col">
          <div class="form-group">
            <a href="{{ base_url("dashboard/museum") }}" class="btn btn-danger btn-lg btn-block">
              <i class="fa fa-close"></i>Cancel
            </a>
          </div>
        </div>
				<div class="col">
					<div class="form-group">
						<button class="btn btn-lg btn-block btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
				</div>
			</div>
		</form>
	</div>
@endsection


@section('script')
	@if(isset($type) && isset($message))
  	<script type="text/javascript">
  		var type = "{{ $type }}";
  		var message = "{{ $message }}"
  	</script>
    <script type="text/javascript" src="{{ base_url("assets/backend/js/toast.js")}}"></script>
  @endif
	<script type="text/javascript" src="{{ base_url("assets/backend/js/museum.js")}}"></script>
@endsection