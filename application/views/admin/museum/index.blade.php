@extends("admin/master/master")

@section("content")
	<section id="posts" class="full-height">
		<div class="container">
			<div class="row">
				<div class="col">
					@if(count($allMuseum) <= 0)
						<div class="text-center">
							<h5 class="display-1 mb-5">Empty Data</h5>
							<a href="{{ base_url("dashboard/museum") }}" class="btn btn-lg btn-primary">
								<i class="fa fa-arrow-circle-left"></i> Go Back To The Earth
							</a>
						</div>
					@else
						<div class="card">
							<div class="card-header">
								<h4>Museum</h4>
							</div>

							<table class="table table-striped">
								<thead class="thead-inverse">
									<tr>
										<th>No</th>
										<th>Nama Museum</th>
										<th>Link</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach($allMuseum as $museum)
										<tr data-set-id="{{ $museum->id }}">
											<td scope="row">{{ $start }} </td>
											<td>{{ $museum->nama_museum }}</td>
											<td>
												<a href="{{ base_url("museum/$museum->slug")}}" target="_blank" class="btn btn-primary btn-sm">
													Detail
												</a>
											</td>
											<td>
												@if($museum->id_user == $login['id'] || $login['id'] == 1 || $login['role'] == 1)
													<a href="{{ base_url("dashboard/museum/{$museum->id}/edit") }}" class="btn btn-secondary">
														<i class="fa fa-angle-double-right"></i> Edit
													</a>
													<button class="btn btn-danger delete-btn"><i class="fa fa-trash"></i> Delete</button>
												@endif
											</td>
										</tr>
										@php
											$start++;
										@endphp
									@endforeach
								</tbody>
							</table>
						</div>
					@endif
				</div>
			</div>
			 @if(count($allMuseum) > 0)
				<div class="row">
					{!! $pagination !!}
				</div>
			@endif
		</div>
	</section>

	<form class="d-none" method="POST" id="set-delete">
	</form>
@endsection

@section('script')
	
	@if(isset($type) && isset($message))
		<script type="text/javascript">
			var type = "{{ $type }}";
			var message = "{{ $message }}"
		</script>
		<script type="text/javascript" src="{{ base_url("assets/backend/js/toast.js")}}"></script>
	@endif
	
	<script type="text/javascript">
		var baseURL = "{{ base_url("dashboard/museum") }}";
	</script>
	<script type="text/javascript" src="{{ base_url("assets/backend/js/popup.js")}}"></script>
@endsection