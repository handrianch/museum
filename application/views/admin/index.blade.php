@extends("admin.master.master")

@section("content")
  <section id="posts" class="full-height">
    <div class="container pt-5">
      <div class="row justify-content-center">
        <div class="col-md-3">
          <div class="card text-center bg-primary text-white mb-3">
            <div class="card-body">
              <h3>Museum</h3>
              <h1 class="display-4"><i class="fa fa-pencil-alt"></i> {{ $museumCount }}</h1>
              <a href="{{ base_url("dashboard/museum") }}" class="btn btn-outline-light btn-sm">View</a>
            </div>
          </div>
        </div>

        <div class="col-md-3">
         <div class="card text-center bg-success text-white mb-3">
            <div class="card-body">
              <h3>Area</h3>
              <h1 class="display-4"><i class="fa fa-pencil-alt"></i> {{ $areaCount }}</h1>
              <a href="{{ base_url("dashboard/area") }}" class="btn btn-outline-light btn-sm">View</a>
            </div>
          </div>
        </div>

        <div class="col-md-3">
          <div class="card text-center bg-warning text-white mb-3">
            <div class="card-body">
              <h3>Users</h3>
              <h1 class="display-4"><i class="fa fa-pencil-alt"></i> {{ $usersCount }}</h1>
              <a href="{{ base_url("dashboard/users") }}" class="btn btn-outline-light btn-sm">View</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@section('script')
  @if(isset($type) && isset($message))
    <script type="text/javascript">
      var type = "{{ $type }}";
      var message = "{{ $message }}"
    </script>
    <script type="text/javascript" src="{{ base_url("assets/backend/js/toast.js")}}"></script>
  @endif
@endsection