@extends("client.master.app")

@section("content")
  <!-- START: section -->
  <section class="probootstrap-intro custom-bg custom-size-show" data-stellar-background-ratio="0.5">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-7 probootstrap-intro-text show-page-margin">
          <h1 class="probootstrap-animate">{{ $museum->nama_museum}}</h1>
          <div class="probootstrap-subtitle probootstrap-animate">
            <h2>Keterangan :</h2>
            {!! $museum->keterangan !!}
            <h2>Alamat :</h2>
            {!! $museum->alamat !!}
            <h2>Jadwal Buka - Tutup :</h2>
            {!! $museum->jadwal_buka_tutup !!}
            <h2>Harga Tiket :</h2>
            {!! $museum->harga_tiket !!}
          </div>
        </div>

        <div class="col-md-5 probootstrap-intro-text">
          <img src="{{ base_url("upload/$museum->gambar")}}" alt="{{ $museum->nama_museum }}" class="img-responsive probootstrap-animate">
        </div>
      </div>
    </div>
  </section>
  <!-- END: section -->
  

 {{--  <section id="next-section" class="probootstrap-section">
    <div class="container">
      <div class="col-md-5 col-md-push-7">
       
        {!! $museum->keterangan !!}
        <p class="mb20">
          <strong class="probootstrap-black-color">Role:</strong> <br>
          Design <br>
          Branding <br>
          Mobile Design
        </p>
        <p class="mb50">
          <strong class="probootstrap-black-color">Client</strong> <br>
          Google, Inc.
        </p>

        <p>
          <a href="#" role="button" class="btn btn-primary">Visit Website</a>
        </p>
      </div>
      <div class="col-md-7 col-md-pull-5">
        <p class="mt-5"><img src="{{ base_url("upload/$museum->gambar")}}" alt="{{ $museum->nama_museum }}" class="img-responsive"></p>
      </div>
    </div>
  </section> --}}
@endsection