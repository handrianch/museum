@extends("client.master.app")

@section("style")
  <link rel="stylesheet" href="{{ base_url("assets/frontend/css/contact.css") }}">
  <link rel="stylesheet" href="{{ base_url("assets/frontend/css/util-contact.css") }}">
@endsection

@section("content")

  <!-- START: section -->
  <section class="probootstrap-intro custom-bg custom-height" data-stellar-background-ratio="0.5">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-7 probootstrap-intro-text">
          <h1 class="probootstrap-animate margin-custom">Kontak</h1>
          <div class="probootstrap-subtitle probootstrap-animate">
            <h2>{!! $settings->contact_text !!}</h2>
          </div>
        </div>

        <div class="col-md-5 custom-margin-top probootstrap-animate">
          <form class="contact100-form validate-form">

          <div class="wrap-input100 validate-input" data-validate = "Name is required">
            <input class="input100 custom-input" type="text" name="name" placeholder="Name" autocomplete="off">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-user" aria-hidden="true"></i>
            </span>
          </div>

          <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
            <input class="input100 custom-input" type="text" name="email" placeholder="Email" autocomplete="off">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-envelope" aria-hidden="true"></i>
            </span>
          </div>

          <div class="wrap-input100 validate-input" data-validate = "Message is required">
            <textarea class="input100 custom-textarea custom-input" name="message" placeholder="Message"></textarea>
            <span class="focus-input100"></span>
          </div>

          <div class="container-contact100-form-btn">
            <button class="contact100-form-btn custom-input">Send</button>
          </div>
        </form>
        </div>
      </div>
    </div>
  </section>
  <!-- END: section -->
@endsection
  
@section("script")
  <script src="{{ base_url("assets/frontend/js/tilt.jquery.min.js") }}"></script>
  <script>
      $('.js-tilt').tilt({
        scale: 1.1
      })
    </script>
  <script src="{{ base_url("assets/frontend/js/contact.js") }}"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-23581568-13');
  </script>
@endsection