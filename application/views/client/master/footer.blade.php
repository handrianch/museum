<footer role="contentinfo" class="probootstrap-footer">
  <div class="container">
    <div class="row mt40">
      <div class="col-md-12 text-center">
        <p>
          <small>&copy; 2018 <a href="https://uicookies.com/" target="_blank">uiCookies:Format</a>. All Rights Reserved. <br> Designed &amp; Developed by <a href="https://uicookies.com/" target="_blank">uicookies.com</a> Edited with <i class="fa fa-heart text-danger"></i> &amp; pain
        </p>
      </div>
    </div>
  </div>
</footer>