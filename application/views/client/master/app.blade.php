<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $settings->nama_website }}</title>
    
  <link rel="stylesheet" href="{{ base_url("assets/backend/css/vendor/bootstrap.min.css") }}">
  <link rel="stylesheet" href="{{ base_url("assets/backend/css/vendor/font-awesome.min.css") }}">
  <link rel="stylesheet" href="{{ base_url("assets/frontend/css/fonts-rubik.css") }}">
  <link rel="stylesheet" href="{{ base_url("assets/frontend/css/montserrat.css") }}">
  <link rel="stylesheet" href="{{ base_url("assets/frontend/css/styles-merged.css") }}">
  <link rel="stylesheet" href="{{ base_url("assets/frontend/css/style.min.css") }}">
  <link rel="stylesheet" href="{{ base_url("assets/frontend/css/custom.css") }}">
    @yield("style")

    <!--[if lt IE 9]>
      <script src="js/vendor/html5shiv.min.js"></script>
      <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!-- START: header -->
      @include("client.master.header")
    <!-- END: header -->
    
    <!-- START: main content -->
      @yield("content")
    <!-- END: main content -->
    
    <!-- START: footer -->
      @yield("footer")
    <!-- END: footer -->

    <!-- START: JS Script -->
      <script src="{{ base_url("assets/frontend/js/scripts.min.js") }}"></script>
      <script src="{{ base_url("assets/frontend/js/main.min.js") }}"></script>
      <script src="{{ base_url("assets/frontend/js/custom.js") }}"></script>
      @yield("script")
    <!-- END: JS Script -->
  </body>
</html>