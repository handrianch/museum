<header role="banner" class="probootstrap-header">
  <div class="container-fluid">
      <a href="{{ base_Url("/") }}" class="probootstrap-logo">{{ $settings->nama_website }}<span>.</span></a>
      
      <a href="#" class="probootstrap-burger-menu visible-xs" ><i>Menu</i></a>
      <div class="mobile-menu-overlay"></div>

      <nav role="navigation" class="probootstrap-nav hidden-xs">
        <ul class="probootstrap-main-nav">
          <li class="{{ $page == 'home' ? "active" : "" }}"><a href="{{ base_url("/") }}">Home</a></li>
          <li class="{{ $page == 'museum' ? "active" : "" }}"><a href="{{ base_url("museum") }}">Museum</a></li>
          <li class="{{ $page == 'tentang' ? "active" : "" }}"><a href="{{ base_url("tentang") }}">Tentang</a></li>
          <li class="{{ $page == 'kontak' ? "active" : "" }}"><a href="{{ base_url("kontak") }}">Kontak</a></li>
        </ul>
        <div class="extra-text visible-xs">
          <a href="#" class="probootstrap-burger-menu"><i>Menu</i></a>
        </div>
      </nav>
  </div>
</header>