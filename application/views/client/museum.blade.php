@extends("client.master.app")
  
@section("content")
  <!-- START: section -->
  <section class="probootstrap-intro custom-bg custom-size-show-fixed" data-stellar-background-ratio="0.5">
    
  </section>
  <!-- END: section -->
  

  <section id="next-section" class="probootstrap-section">
    <div class="container">
      <div class="row">
        <div class="col-md-7 probootstrap-intro-text">
          <h1 class="probootstrap-animate">Daftar Museum</h1>
        </div>
      </div>
      <div class="row">
        @foreach($allMuseum as $museum)
          <div class="col-md-4 col-sm-6 probootstrap-animate">
              <div class="probootstrap-block-image">
                <figure class="figure-wrap"><img src="{{ base_url("upload/$museum->gambar")}}" alt="{{ $museum->nama_museum }}" class="custom-img"></figure>
                <div class="text">
                  <h4 class="mb20 mt0"><a href="{{ base_url("museum/$museum->slug/show") }}">{{ $museum->nama_museum }}</a></h4>
                  <p class="dark">{!! substr($museum->keterangan, 0, 90) !!} ...</p>
                </div>
              </div>
          </div>
          @if($loop->iteration % 2 == 0)
            <div class="clearfix visible-sm-block"></div>
          @endif
        @endforeach
      </div>
    </div>

    <div class="container">
       <div class="row">
          {!! $pagination !!}
        </div>
    </div>
  </section>

  @include("client.master.footer")
@endsection