@extends("client.master.app")
  
@section("content")
  <!-- START: section -->
  <section class="probootstrap-intro custom-bg custom-height" data-stellar-background-ratio="0.5">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-7 probootstrap-intro-text">
          <h1 class="probootstrap-animate">Tentang</h1>
          <div class="probootstrap-subtitle probootstrap-animate">
            <h2>{!! $settings->about_text !!}</h2>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- END: section -->
@endsection